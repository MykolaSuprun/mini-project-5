package com.example.mini_project_5;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.widget.RemoteViews;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import static com.example.mini_project_5.StaticMediaPlayer.player;

/**
 * Implementation of App Widget functionality.
 */
public class ImageViewWidget extends AppWidgetProvider {

    public static String NEXT_IMAGE_BUTTON = "nextImageButtonTag";
    public static String PLAY_SONG_BUTTON = "playSongButtonTag";
    public static String PAUSE_SONG_BUTTON = "pauseSongButtonTag";
    public static String STOP_SONG_BUTTON = "stopSongButtonTag";
    public static String NEXT_SONG_BUTTON = "nextSongButtonTag";
    static RemoteViews views;
    public static Boolean imageFlag = false;
    public static Boolean songFlag = true;

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        //CharSequence widgetText = context.getString(R.string.appwidget_text);
        // Construct the RemoteViews object
        views = new RemoteViews(context.getPackageName(), R.layout.image_view_widget);
        Intent intent = new Intent(NEXT_IMAGE_BUTTON);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent,
//                PendingIntent.FLAG_UPDATE_CURRENT);
//        views.setOnClickPendingIntent(R.id.buttonNext, pendingIntent );

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    protected PendingIntent getPendingSelfIntent(Context context, String action) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        ComponentName thisWidget = new ComponentName(context, ImageViewWidget.class);
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

        for (int widgetId : allWidgetIds) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.image_view_widget);
            remoteViews.setOnClickPendingIntent(R.id.buttonNext, getPendingSelfIntent(context, NEXT_IMAGE_BUTTON));
            remoteViews.setOnClickPendingIntent(R.id.buttonPlay, getPendingSelfIntent(context, PLAY_SONG_BUTTON));
            remoteViews.setOnClickPendingIntent(R.id.buttonPause, getPendingSelfIntent(context, PAUSE_SONG_BUTTON));
            remoteViews.setOnClickPendingIntent(R.id.buttonStop, getPendingSelfIntent(context, STOP_SONG_BUTTON));
            remoteViews.setOnClickPendingIntent(R.id.nextSongButton, getPendingSelfIntent(context, NEXT_SONG_BUTTON));
            if (imageFlag)
                remoteViews.setImageViewResource(R.id.imageView, R.drawable.im1);
            else
                remoteViews.setImageViewResource(R.id.imageView, R.drawable.im2);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        super.onReceive(context, intent);
        AppWidgetManager appWidgetManager = AppWidgetManager
                .getInstance(context);

        if (NEXT_IMAGE_BUTTON.equals(intent.getAction())) {
            imageFlag = !imageFlag;
            ComponentName thisAppWidget = new ComponentName(
                    context.getPackageName(), getClass().getName());
            int ids[] = appWidgetManager.getAppWidgetIds(thisAppWidget);
            onUpdate(context, appWidgetManager, ids);
        }
        if (PLAY_SONG_BUTTON.equals(intent.getAction())) {
            if (player == null) {
                Intent songIntent = new Intent(context,MusicService.class);
                songIntent.putExtra("Song",songFlag);
                context.startService(songIntent);
            } else
                player.start();
        }
        if (PAUSE_SONG_BUTTON.equals(intent.getAction())) {
            if (player != null) {
                player.pause();
            }
        }
        if (STOP_SONG_BUTTON.equals(intent.getAction())) {
            context.stopService(new Intent(context, MusicService.class));
        }
        if (NEXT_SONG_BUTTON.equals(intent.getAction())) {
            songFlag = !songFlag;
            context.stopService(new Intent(context, MusicService.class));
            Intent songIntent = new Intent(context,MusicService.class);
            songIntent.putExtra("Song",songFlag);
            context.startService(songIntent);

            Toast.makeText(context, "Next song button", Toast.LENGTH_SHORT).show();
            System.out.println("Next song button");
        }
    }


    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

}

