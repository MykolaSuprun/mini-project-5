package com.example.mini_project_5;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import static com.example.mini_project_5.StaticMediaPlayer.player;

public class MusicService extends Service {
//    private StaticMediaPlayer player;
    public MusicService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Boolean flag = intent.getBooleanExtra("Song",true);
        if(flag)
             player = MediaPlayer.create(this, R.raw.song3);
        else
            player = MediaPlayer.create(this, R.raw.song1);

        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stopPlayer();
            }
        });
        player.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        stopPlayer();
    }
    private void stopPlayer() {
        if (player != null) {
            player.stop();
            player.release();
            player = null;
        }
    }

}
